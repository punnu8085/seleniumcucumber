package Pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utils.CommonUtils;
import utils.Driver;

public class AuthenticationPage extends Driver {
    private Logger logger;
    CommonUtils obj;

    @FindBy(xpath = "//input[@id='email']")
    WebElement txtemailadd;

    @FindBy(xpath = "//input[@id='passwd']")
    WebElement txtpassword;

    @FindBy(xpath = "//button[@id='SubmitLogin']")
    WebElement btnsignin;


    public AuthenticationPage()
    {
        logger=Logger.getLogger(this.getClass());
        obj = new CommonUtils();
        PageFactory.initElements(driver,this);
    }

    //enter email address
    public void enterEmailAdd(String email)
    {
        obj.enterText(txtemailadd,email);
        logger.info("user enter email address");
    }

    //enter Password
    public void enterPassword(String password)
    {
        obj.enterText(txtpassword,password);
        logger.info("User enter password");
    }

    //click on Sign In Button
    public void clickSignInBtn()
    {
        obj.clickElement(btnsignin);
        logger.info("User clicked on Sign In Button");
    }
}
