package Pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utils.CommonUtils;
import utils.Driver;
import utils.ReadProperties;

import java.util.concurrent.TimeUnit;

public class BasePage extends Driver {
    private Logger logger;

    @FindBy(xpath = "//a[@class='login'][contains(text(),'Sign in')]")
    WebElement linksignin;

    public BasePage()
    {
        logger=Logger.getLogger(this.getClass());
        obj = new CommonUtils();
        PageFactory.initElements(driver,this);
       // obj.waitForPageLoad(driver);
    }

    //open URL
    public  BasePage OpenURL() throws Exception {
        ReadProperties.readProperties();
        String  URL=ReadProperties.URL;
        driver.navigate().to(URL);
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        System.out.println("URL opened Successfully");
        return (new BasePage());
    }
    //click on Sign in Link from Header Menu
    public  void clickSignInFromMenu()
    {
    obj.clickElement(linksignin);
    logger.info("User clicked on Sign In Link from header menu");
    }
}
