package utils;

import org.apache.log4j.Logger;
import org.checkerframework.checker.nullness.compatqual.NullableDecl;
import org.junit.Assert;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.*;

import java.util.List;


public class CommonUtils extends Driver {
    private Logger logger;

    public CommonUtils()
    {
        //constructor
    }

    //click on a webElement
    public void clickElement(WebElement element) {

       // WaitStatementLib.eWaitForVisible( driver,20, element);
        if (element.isEnabled()) {
            element.click();
        } else {
          //  logger.info("Element is not enabled");
            Assert.fail();

        }
    }
    //Wait for page load
    public static void waitForPageLoad(WebDriver driver)
    {
        ExpectedCondition<Boolean> expectation = new ExpectedCondition<Boolean>() {
            @NullableDecl
            @Override
            public Boolean apply(@NullableDecl WebDriver driver) {
                return ((JavascriptExecutor) driver).executeScript("return document ready state").equals("complete");
            }
        };
        Wait<WebDriver> wait = new WebDriverWait(driver,30);
        try
        {
            wait.until(expectation);
        }
        catch (Throwable error)
        {
            error.printStackTrace();
        }
    }
    //enter text in a WebElement
    public void enterText(WebElement element,String text)
    {
        //WaitStatementLib.eWaitForVisible(driver,20,element);
        if(element.isEnabled())
        {
        element.click();
        element.sendKeys(Keys.CONTROL + "a");
        element.sendKeys(Keys.DELETE);
        element.sendKeys(text);
    }
    else {
                logger.info("Element is not enabled");
            Assert.fail();
        }

    }

    //get current method name
    public  static String getMethodName()
    {
        StackTraceElement[] stackTrac = Thread.currentThread().getStackTrace();
        StackTraceElement e= stackTrac[1];
        return e.getMethodName();
    }

    //get attribute value from a element
    public String getElementText(WebElement element)
    {
        String value = null;
        //WaitStatementLib.eWaitForVisible(driver,20,element);
        if(element.isEnabled())
        {
            value=  element.getAttribute("value");
        }
        else{
            logger.info("Element is not enabled");
            Assert.fail();
        }
        return value;
    }
  //get text from  webelement
 public String getText(WebElement element)
 {
     String text=null;
     //WaitStatementLib.eWaitForVisible(driver,20,element);
     if(element.isEnabled())
     {
         text = element.getText();
     }
     else{
         logger.info("Element is not enabled");
         Assert.fail();
     }
 return text;
 }

 //select radio button
    public void SelectRadioButton(WebElement element)
    {
       // WaitStatementLib.eWaitForVisible(driver,20,element);
        if(element.isSelected())
        {
            logger.info("Radio button is already select");
        }
        else{
            element.click();
        }
    }

    //select CheckBox
    public void SelectCheckBox(WebElement element)
    {
        //WaitStatementLib.eWaitForVisible(driver,20,element);
        if(element.isSelected())
        {
            logger.info("CheckBox is already select");
        }
        else{
            element.click();
        }

    }
    //Select dropdown value by visible text
    public void selectDDByVisibleText(WebElement element,String visibletext)
    {
        //WaitStatementLib.eWaitForVisible(driver,20,element);
        Select sl = new Select(element);
        sl.selectByVisibleText(visibletext);
    }

    //Select dropdown value by  text
    public void selectDDByValue(WebElement element,String text)
    {
        //WaitStatementLib.eWaitForVisible(driver,20,element);
        Select sl = new Select(element);
        sl.selectByValue(text);
    }
    //Select dropdown value by Index
    public void selectDDByIndex(WebElement element,int index)
    {
        //WaitStatementLib.eWaitForVisible(driver,20,element);
        Select sl = new Select(element);
        sl.selectByIndex(index);
    }

    //check is dropdown multiple select or not
    public void isMultiple(WebElement element)
    {
        //WaitStatementLib.eWaitForVisible(driver,20,element);
        Select sl = new Select(element);
        sl.isMultiple();
    }

    //get all dropdown values
    public String  getDDValues(WebElement element)
    {
        //WaitStatementLib.eWaitForVisible(driver,20,element);
        Select sl = new Select(element);
        String sloptions= null;

        List<WebElement> options = sl.getOptions();
        for(int i=0;i<=options.size();i++)
        {
            sloptions= options.get(i).getText();
        }
return  sloptions;
    }

    //get selected dd value
    public String getSelectedDDValue(WebElement element)
    {
        //WaitStatementLib.eWaitForVisible(driver,20,element);
        Select sl = new Select(element);
        return sl.getFirstSelectedOption().getText();

    }



    //Mouse Overing to  a Element
    public void MoveToElement(WebElement element)
    { Actions ac= new Actions(driver);
        ac.moveToElement(element).perform();
    }

    //Double Click on a element
    public void DoubleClick(WebElement element)
    { Actions ac= new Actions(driver);
        ac.doubleClick(element).perform();
    }

    //right click on a element
    public void RightClick(WebElement element)
    {
        Actions ac= new Actions(driver);
        ac.contextClick(element).perform();
    }


}


