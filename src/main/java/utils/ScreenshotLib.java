package utils;


import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import java.io.File;
import java.io.IOException;
public class ScreenshotLib  {

    public byte[] takeScreenshot()
    {
        WebDriver driver =Driver.driver;
return ((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES);
    }


}
