package utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

import java.util.logging.Logger;

public class DriverFactory
{


    private static Logger logger;

    public static WebDriver LaunchBrowser() throws Exception {
        ReadProperties.readProperties();
        String browserName = ReadProperties.Browser;
        if (browserName.equalsIgnoreCase("Firefox")) {
            System.setProperty("webdriver.gecko.driver", "./drivers/geckodriver.exe");
            return new FirefoxDriver();
        } else if (browserName.equalsIgnoreCase("chrome")) {
            System.setProperty("webdriver.chrome.driver", "./chromedriver/chromedriver.exe");
            return new ChromeDriver();
        } else if (browserName.equalsIgnoreCase("ie")) {
            System.setProperty("webdriver.ie.driver", "./drivers/IEDriverServer.exe");
            return new InternetExplorerDriver();
        }

        return null;
    }

    public static void tearDown(WebDriver driver)
    {

        driver.quit();
        logger.info("Browser closed successfully");


    }

}
