package utils;


import org.openqa.selenium.WebDriver;

public class Driver extends ScreenshotLib
{
    public static CommonUtils obj;
    public static WebDriver driver;

public void initializeDriver() throws Exception {

if(driver==null)
{   driver = DriverFactory.LaunchBrowser();
    System.out.println("Browser Opened Successfully");

}
else
{
 System.out.println("-----------------------------Driver Already Initialized");
}
}

public void closeDriver()
{
    System.out.println(Driver.class.getName()+"-------------------Close Driver--------------");
    DriverFactory.tearDown(driver);
    System.out.println("-------Driver closed Successfully--------------");

}
}
