package utils;


import cucumber.api.java.After;
import cucumber.api.java.Before;

public class Hooks {

    Driver driver=new Driver();
    @Before
    public void beforeFeature() throws Exception {
        driver.initializeDriver();
    }

    @After
    public void AfterFeature()
    {
        driver.closeDriver();
    }
}
