package StepDef;


import Pages.AuthenticationPage;
import Pages.BasePage;
import Pages.MyAccountPage;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import java.util.logging.Logger;

public class LoginStepDef{

    AuthenticationPage ap = new AuthenticationPage();
    MyAccountPage mp = new MyAccountPage();
    BasePage bp =new BasePage();
private Logger logger;

    @Given("^I am present on Home page$")
    public void i_am_present_on_Home_page() throws Throwable {
        System.out.println("------------------");
    }

    @When("^I  click on Sign In menu$")
    public void i_click_on_Sign_In_menu() throws Throwable {
        System.out.println("------------------");
    }

    @When("^I enter email address as \"([^\"]*)\"$")
    public void i_enter_email_address_as(String arg1) throws Throwable {
        System.out.println("------------------");
    }

    @When("^I enter Password as \"([^\"]*)\"$")
    public void i_enter_Password_as(String arg1) throws Throwable {
        System.out.println("------------------");
    }

    @When("^I click on Sign In Button$")
    public void i_click_on_Sign_In_Button() throws Throwable {
        System.out.println("------------------");
    }

    @Then("^I loggedIn into Application$")
    public void i_loggedIn_into_Application() throws Throwable {
    System.out.println("------------------");
    }


}
